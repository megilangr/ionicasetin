import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/auth/login/login.module').then( m => m.LoginPageModule)
  },
  // {
  //   path: 'landing',
  //   loadChildren: () => import('./pages/landing/landing.module').then( m => m.LandingPageModule)
  // }
  {
    path: 'landing',
    loadChildren: './pages/landing/landing.module#LandingPageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'workorder',
    loadChildren: () => import('./pages/workorder/workorder.module').then( m => m.WorkorderPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'aset',
    loadChildren: () => import('./pages/aset/aset.module').then( m => m.AsetPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'profile',
    loadChildren: () => import('./pages/profile/profile.module').then( m => m.ProfilePageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'request',
    loadChildren: () => import('./pages/order/request/request.module').then( m => m.RequestPageModule),
  },


  // {
  //   path: 'aset',
  //   children: [
  //     {
  //       path: '',
  //       loadChildren: () => import('./pages/aset/aset.module').then( m => m.AsetPageModule)
  //     }
  //   ],
  //   canActivate: [AuthGuard]
  // }


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
