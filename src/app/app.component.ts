import { Component } from '@angular/core';

import { Platform, MenuController, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './services/auth.service';
import { Storage } from '@ionic/storage';
import { AlertService } from './services/alert.service';
import { Router } from '@angular/router';
import { LoadingService } from './services/loading.service';
import { WorkorderService } from './services/workorder.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  userName: any = null;
  userStatus: any = null;

  public appPages = [
    {
      title: 'Halaman Utama',
      url: '/landing',
      icon: 'home'
    },
    {
      title: 'Workorder',
      url: '/workorder',
      icon: 'analytics'
    },
    {
      title: 'Data Aset',
      url: '/aset',
      icon: 'list-box'
    },
    {
      title: 'Order',
      url: '/request',
      icon: 'options'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AuthService,
    private alert: AlertService,
    private router: Router,
    private loading: LoadingService,
    private woService: WorkorderService,
    private storage: Storage,
    private navCtrl: NavController,
    private menuCtrl: MenuController,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.authService.getToken();
      if (this.loading.normalLoad == null) {
        this.loading.loadingSetup();
      }
      if (this.woService.wo == null) {
        this.woService.getWo();
      }
    });
  }

  logout() {
    this.loading.normalLoad.present();
    this.authService.logout(this.authService.token).subscribe(
      data => {
        console.log(data);
        this.alert.alertToast('Berhasil Melakukan Logout !');
      }, error => {
        this.alert.alertToast('Terjadi Kesalahan !');
      }, () => {
        this.loading.normalLoad.dismiss();
        this.router.navigate(['/login']);
      }
    );
  }

  getUser() {
    this.storage.get('user').then(data => {
      if (data) {
        this.userName = data['name'];
        this.userStatus = data['status'];
      }
    });
  }

  openProfile() {
    this.navCtrl.navigateRoot('/profile');
    this.menuCtrl.close();
  }
}
