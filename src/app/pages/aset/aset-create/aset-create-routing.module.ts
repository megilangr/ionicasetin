import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AsetCreatePage } from './aset-create.page';

const routes: Routes = [
  {
    path: '',
    component: AsetCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AsetCreatePageRoutingModule {}
