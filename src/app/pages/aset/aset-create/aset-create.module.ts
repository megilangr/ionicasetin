import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AsetCreatePageRoutingModule } from './aset-create-routing.module';

import { AsetCreatePage } from './aset-create.page';
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AsetCreatePageRoutingModule,
    IonicSelectableModule,
  ],
  declarations: [AsetCreatePage]
})
export class AsetCreatePageModule {}
