import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AsetCreatePage } from './aset-create.page';

describe('AsetCreatePage', () => {
  let component: AsetCreatePage;
  let fixture: ComponentFixture<AsetCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsetCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AsetCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
