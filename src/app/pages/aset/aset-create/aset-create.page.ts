import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingService } from 'src/app/services/loading.service';
import { NavController, Platform } from '@ionic/angular';
import { AsetService } from 'src/app/services/aset.service';
import { AlertService } from 'src/app/services/alert.service';
import { WorkorderService } from 'src/app/services/workorder.service';
import { IonicSelectableComponent } from 'ionic-selectable';
import { File } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Base64 } from '@ionic-native/base64/ngx';

@Component({
  selector: 'app-aset-create',
  templateUrl: './aset-create.page.html',
  styleUrls: ['./aset-create.page.scss'],
})
export class AsetCreatePage implements OnInit {
  qr: any = null;

  aset: any = {
    owner_id: undefined,
    condition: undefined,
    serial_number: undefined,
    accounting_number: undefined,
    year_purchase: undefined,
    notes: undefined,
    asset_value: undefined,
    location_id: undefined,
    site_id: undefined,
    building_id: undefined,
    room_id: undefined,
    type_id: undefined,
    category_id: undefined,
    brand_id: undefined,
    model_id: undefined,
    client_id: undefined,
    is_active: undefined,
    created_by: undefined,
    updated_by: undefined,
    created_at: undefined,
    updated_at: undefined,
  };

  base64image: any = null;
  imageAset: any = null;
  uploadImg: any = null;

  dataModel: any = null;
  dataOwner: any = null;
  dataBrand: any = null;
  dataRoom: any = null;


  constructor(
    private activatedRoute: ActivatedRoute,
    private loading: LoadingService,
    private navCtrl: NavController,
    private asetService: AsetService,
    private alert: AlertService,
    private woService: WorkorderService,
    private webView: WebView,
    private file: File,
    private camera: Camera,
    private base64: Base64,
    private plt: Platform,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.loading.customLoading(3000);
    if (this.woService.wo === null) {
      this.navCtrl.navigateRoot('/workorder');
    } else {
      this.getParamater();

      setTimeout(() => {
        this.getRoom();
        this.getBrand();
        this.getOwner();
      }, 1000);
    }
  }

  ionViewDidEnter() {
    //
  }

  getParamater() {
    this.activatedRoute.paramMap.subscribe(
      param => {
        if (!param.has('qrCode')) {
          return this.navCtrl.navigateRoot('/aset');
        } else {
          this.asetService.qrCodeStatus(param.get('qrCode')).subscribe(
            data => {
              let a: any = data;
              if (a.status === 'notfound') {
                this.alert.alertToast('Qr Code Tidak di-Temukan ! \nSilahkan Scan Ulang QR !');
                this.navCtrl.navigateRoot('/aset');
              } else if (a.status === 'idle') {
                this.alert.alertToast('Qr Code Tidak di-Temukan ! \nSilahkan Scan Ulang QR !');
                this.navCtrl.navigateRoot('/aset');
              } else if (a.status === 'used') {
                this.alert.alertToast('Qr Code Sudah di-Gunakan ! \nSilahkan Scan Ulang QR !');
                this.navCtrl.navigateRoot('/aset');
              } else if (a.status === 'booked') {
                return this.qr = param.get('qrCode');
              }
            }
          );
        }
      }
    );
  }

  createAset(form) {
    this.loading.customLoading(3000);
    this.asetService.createAset(
      form.value.qrCode,
      form.value.condition,
      form.value.serialNumber,
      form.value.accountingNumber,
      form.value.yearPurchase,
      form.value.notes,
      form.value.assetValue,
      form.value.brandId.id,
      form.value.modelId.id,
      form.value.roomId.id,
      form.value.ownerId.id,
      this.base64image
      ).subscribe(
        data => {
          this.alert.alertToast('Data Aset di-Buat !');
          setTimeout(() => {
            this.navCtrl.navigateRoot('/aset');
            this.loading.customLoad.dismiss();
          }, 2000);
        }
      );
  }

  getRoom() {
    this.asetService.room(this.woService.wo.building_id).subscribe(
      data => {
        this.dataRoom = data;
        setTimeout(() => {
          // console.log(this.dataRoom);
        }, 1000);
      }
    );
  }

  getBrand() {
    this.asetService.brand().subscribe(
      data => {
        this.dataBrand = data;
        setTimeout(() => {
          // console.log('Log dataBrand : ' + this.dataBrand);
        }, 1000);
      }
    );
  }

  getModel() {
    this.asetService.model(this.aset.brand_id).subscribe(
      data => {
        this.dataModel = data;
        setTimeout(() => {
          // console.log('Log dataModel : ' + this.dataModel);
        }, 2000);
      }
    );
  }

  getOwner() {
    this.asetService.owner().subscribe(
      data => {
        this.dataOwner = data;
        setTimeout(() => {
          // console.log('Log dataOwner : ' + data);
        }, 1000);
      }
    );
  }

  model(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    this.asetService.model(event.value.id).subscribe(
      data => {
        this.aset.model_id = null;
        this.dataModel = data;
        setTimeout(() => {
          // console.log(this.dataModel);
        });
      }
    );
  }

  cameraUp() {
    this.plt.ready().then(() => {
      const options: CameraOptions =  {
        quality: 50,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        saveToPhotoAlbum: true
      };

      this.camera.getPicture(options).then(
        (imageData) => {
          const filePath: string = imageData;
          this.base64.encodeFile(filePath).then((base64file: string) => {
            this.base64image = base64file;
            console.log(this.base64image);

            const currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
            const correctPath = imageData.substr(0, imageData.lastIndexOf('/') + 1);
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());

            setTimeout(() => {
              // console.log(this.base64image);
            }, 1000);
          }, (err) => {
            this.alert.alertToast('Terjadi Kesalahan - Capture Image X-1344');
          });
        }, (err) => {
          console.log('Terjadi Kesalahan !' + err);
        }
      );
    });
  }

  // CAMERA

  pathForImg(img) {
    if (img === null) {
      return '';
    } else {
      const converted = this.webView.convertFileSrc(img);
      console.log('Function Converted :' + converted);
      return converted;
    }
  }

  createFileName() {
    const d = new Date(),
          n = d.getTime(),
          newFileName = n + '.jpg';
    return newFileName;
  }

  copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(
      _ => {
        console.log('Success Copy : ' + _);
        this.updateStoredImages(newFileName);
      }, error => {
        console.log(error);
      }
    );
  }

  updateStoredImages(Name) {
    const FilePath = this.file.dataDirectory + Name;
    const resPath = this.pathForImg(FilePath);

    this.uploadImg = {
      name: Name,
      path: resPath,
      filePath: FilePath
    };

    console.log(this.uploadImg);
  }

  // Camera End

}
