import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AsetDetailPageRoutingModule } from './aset-detail-routing.module';

import { AsetDetailPage } from './aset-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AsetDetailPageRoutingModule
  ],
  declarations: [AsetDetailPage]
})
export class AsetDetailPageModule {}
