import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AsetDetailPage } from './aset-detail.page';

describe('AsetDetailPage', () => {
  let component: AsetDetailPage;
  let fixture: ComponentFixture<AsetDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsetDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AsetDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
