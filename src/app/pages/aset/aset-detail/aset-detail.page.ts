import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { LoadingService } from 'src/app/services/loading.service';
import { AsetService } from 'src/app/services/aset.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-aset-detail',
  templateUrl: './aset-detail.page.html',
  styleUrls: ['./aset-detail.page.scss'],
})
export class AsetDetailPage implements OnInit {
  asetId: any = null;
  // detailAset: any = null;

  link = '/aset/';

  detailAset: any = {
    id: undefined,
    qr_code: undefined,
    owner_id: undefined,
    condition: undefined,
    serial_number: undefined,
    accounting_number: undefined,
    year_purchase: undefined,
    notes: undefined,
    asset_value: undefined,
    location_id: undefined,
    site_id: undefined,
    building_id: undefined,
    room_id: undefined,
    type_id: undefined,
    category_id: undefined,
    brand_id: undefined,
    model_id: undefined,
    client_id: undefined,
    is_active: undefined,
    created_by: undefined,
    updated_by: undefined,
    created_at: undefined,
    updated_at: undefined,
  };

  imageAset: any = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private navCtrl: NavController,
    private loading: LoadingService,
    private asetService: AsetService,
    private sanitizer: DomSanitizer,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.loading.customLoading(3000);
    this.getParameter();
    setTimeout(() => {
      this.getDetail();
    }, 2000);
  }

  getParameter() {
    this.activatedRoute.paramMap.subscribe(
      param => {
        if (!param.has('asetId')) {
          return this.navCtrl.navigateRoot('/aset');
        } else {
          return this.asetId = param.get('asetId');
        }
      }
    );
  }

  getDetail() {
    this.asetService.detailAset(this.asetId).subscribe(
      data => {
        this.detailAset = data;
        this.asetService.getImage(this.asetId).subscribe(
          data1 => {
            let a: any = data1;
            if (a.length === 0) {
              this.imageAset = null;
            } else {
              this.imageAset = data1;
            }
            console.log(data1);
          }
        );
        console.log(data);
      }
    );
  }

  transform(url: any) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
