import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AsetEditPage } from './aset-edit.page';

const routes: Routes = [
  {
    path: '',
    component: AsetEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AsetEditPageRoutingModule {}
