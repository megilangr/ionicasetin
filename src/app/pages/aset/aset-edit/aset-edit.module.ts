import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AsetEditPageRoutingModule } from './aset-edit-routing.module';

import { AsetEditPage } from './aset-edit.page';
import { IonicSelectableModule } from 'ionic-selectable';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AsetEditPageRoutingModule,
    IonicSelectableModule
  ],
  declarations: [AsetEditPage]
})
export class AsetEditPageModule {}
