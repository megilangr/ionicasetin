import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AsetEditPage } from './aset-edit.page';

describe('AsetEditPage', () => {
  let component: AsetEditPage;
  let fixture: ComponentFixture<AsetEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsetEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AsetEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
