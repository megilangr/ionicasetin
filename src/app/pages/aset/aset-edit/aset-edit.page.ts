import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, Platform } from '@ionic/angular';
import { LoadingService } from 'src/app/services/loading.service';
import { AsetService } from 'src/app/services/aset.service';
import { NgForm } from '@angular/forms';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { AlertService } from 'src/app/services/alert.service';
import { WorkorderService } from 'src/app/services/workorder.service';
import { IonicSelectableComponent } from 'ionic-selectable';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { File } from '@ionic-native/file/ngx';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-aset-edit',
  templateUrl: './aset-edit.page.html',
  styleUrls: ['./aset-edit.page.scss'],
})
export class AsetEditPage implements OnInit {
  asetId: any = null;
  // detailAset: any = null;

  detailAset: any = {
    id: undefined,
    qr_code: undefined,
    owner_id: undefined,
    condition: undefined,
    serial_number: undefined,
    accounting_number: undefined,
    year_purchase: undefined,
    notes: undefined,
    asset_value: undefined,
    location_id: undefined,
    site_id: undefined,
    building_id: undefined,
    room_id: undefined,
    type_id: undefined,
    category_id: undefined,
    brand_id: undefined,
    model_id: undefined,
    client_id: undefined,
    is_active: undefined,
    created_by: undefined,
    updated_by: undefined,
    created_at: undefined,
    updated_at: undefined,
  };

  dataRoom: any = null;
  dataBrand: any = null;
  dataModel: any = null;
  dataOwner: any = null;

  base64image: any = null;
  uploadImg: any = null;
  imageAset: any = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private navCtrl: NavController,
    private loading: LoadingService,
    private asetService: AsetService,
    private plt: Platform,
    private barcodeScanner: BarcodeScanner,
    private alert: AlertService,
    private woService: WorkorderService,
    private camera: Camera,
    private base64: Base64,
    private webView: WebView,
    private file: File,
    private sanitizer: DomSanitizer,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.loading.customLoading(3000);
    this.getParameter();
    setTimeout(() => {
      this.getDetail();
    }, 1000);
  }

  ionViewDidEnter() {
    setTimeout(() => {
      this.getRoom();
      this.getBrand();
      this.getModel();
      this.getOwner();
    }, 1500);

    setTimeout(() => {
      this.selectBrand(this.detailAset.brand_id);
      this.selectModel(this.detailAset.model_id);
    }, 2500);

    setTimeout(() => {
      this.selectRoom(this.detailAset.room_id);
      this.selectOwner(this.detailAset.owner_id);
    }, 2500);

  }

  getParameter() {
    this.activatedRoute.paramMap.subscribe(
      param => {
        if (!param.has('asetId')) {
          return this.navCtrl.navigateRoot('/aset');
        } else {
          return this.asetId = param.get('asetId');
        }
      }
    );
  }

  getDetail() {
    this.asetService.detailAset(this.asetId).subscribe(
      data => {
        this.detailAset = data;
        this.getImage();
        console.log(data);
      }
    );
  }

  getImage() {
    this.asetService.getImage(this.asetId).subscribe(
      data => {
        let a: any = data;
        if (a.length === 0) {
          this.imageAset = null;
        } else {
          this.imageAset = data;
          if (this.imageAset.length > 0 ) {
            this.base64image = this.imageAset[0].asset_image;
          }
        }
      }
    );
  }

  makeUpdate(form: NgForm) {
    this.loading.customLoading(3000);
    console.log(form.value);
    this.asetService.updateAset(
      this.detailAset.id,
      form.value.qrCode,
      form.value.condition,
      form.value.serialNumber,
      form.value.accountingNumber,
      form.value.yearPurchase,
      form.value.notes,
      form.value.assetValue,
      form.value.brandId.id,
      form.value.modelId.id,
      form.value.roomId.id,
      form.value.ownerId.id,
      this.base64image
      ).subscribe(
        data => {
          console.log(data);
          this.alert.alertToast('Data Aset di-Ubah !');
          setTimeout(() => {
            this.navCtrl.navigateRoot('/aset/' + this.detailAset.id);
          }, 2000);
        }
      );
  }

  scanQr() {
    this.plt.ready().then(() => {
      this.barcodeScanner.scan().then(data => {
        let a = data;
        if (a.cancelled === false) {
          this.asetService.qrCodeStatus(a.text).subscribe(
            data1 => {
              let b: any = data1;
              if (b.status === 'notfound') {
                this.alert.alertToast('Pemberitahuan ! \n' + b.message);
              } else if (b.status === 'idle') {
                this.alert.alertToast('Pemberitahuan ! \n' + b.message);
              } else if (b.status === 'used') {
                this.alert.alertToast('Pemberitahuan ! \n' + 'QR Code Sudah di-Gunakan Oleh Aset Lain !');
              } else if (b.status === 'booked') {
                this.detailAset.qr_code = a.text;
              }
            }
          );
        } else {
          this.detailAset.qr_code = null;
        }
      }).catch(err => {
        this.alert.alertToast('Terjadi Kesalahan - Scan Barcode X-211');
        console.log('Terjadi Kesalahan Scan !' + err);
      });
    });
  }

  getRoom() {
    this.asetService.room(this.woService.wo.building_id).subscribe(
      data => {
        this.dataRoom = data;
        setTimeout(() => {
          // console.log(this.dataRoom);
        }, 1000);
      }
    );
  }

  getBrand() {
    this.asetService.brand().subscribe(
      data => {
        this.dataBrand = data;
        setTimeout(() => {
          // console.log('Log dataBrand : ' + this.dataBrand);
        }, 1000);
      }
    );
  }

  getModel() {
    this.asetService.model(this.detailAset.brand_id).subscribe(
      data => {
        this.dataModel = data;
        setTimeout(() => {
          // console.log('Log dataModel : ' + this.dataModel);
        }, 2000);
      }
    );
  }

  getOwner() {
    this.asetService.owner().subscribe(
      data => {
        this.dataOwner = data;
        setTimeout(() => {
          // console.log('Log dataOwner : ' + data);
        }, 1000);
      }
    );
  }

  selectRoom(roomId: any) {
    if (roomId !== null) {
      let roomCb = this.dataRoom.filter(items => items.id === roomId);
      return this.detailAset.room_id = { name: roomCb[0].name, id: roomCb[0].id };
    }
  }

  selectBrand(brandId: any) {
    if (brandId !== null) {
      let brandCb = this.dataBrand.filter(items => items.id === brandId);
      return this.detailAset.brand_id = { name: brandCb[0].name, id: brandCb[0].id };
    }
  }

  selectModel(modelId: any) {
    if (modelId !== null) {
      let modelCb = this.dataModel.filter(items => items.id === modelId);
      return this.detailAset.model_id = { name: modelCb[0].name, id: modelCb[0].id };
    }
  }

  selectOwner(ownerId: any) {
    if (ownerId !== null) {
      let ownerCb = this.dataOwner.filter(items => items.id === ownerId);
      return this.detailAset.owner_id = { name: ownerCb[0].name, id: ownerCb[0].id };
    }
  }

  model(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    this.asetService.model(event.value.id).subscribe(
      data => {
        this.detailAset.model_id = null;
        this.dataModel = data;
        setTimeout(() => {
          // console.log(this.dataModel);
        });
      }
    );
  }


  cameraUp() {
    this.plt.ready().then(() => {
      const options: CameraOptions =  {
        quality: 50,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        saveToPhotoAlbum: true
      };

      this.camera.getPicture(options).then(
        (imageData) => {
          const filePath: string = imageData;
          this.base64.encodeFile(filePath).then((base64file: string) => {
            this.base64image = base64file;

            const currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
            const correctPath = imageData.substr(0, imageData.lastIndexOf('/') + 1);
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());

            setTimeout(() => {
              // console.log(this.base64image);
            }, 1000);
          }, (err) => {
            this.alert.alertToast('Terjadi Kesalahan - Capture Image X-1344');
          });
        }, (err) => {
          console.log('Terjadi Kesalahan !' + err);
        }
      );
    });
  }

  // CAMERA

  pathForImg(img) {
    if (img === null) {
      return '';
    } else {
      const converted = this.webView.convertFileSrc(img);
      console.log('Function Converted :' + converted);
      return converted;
    }
  }

  createFileName() {
    const d = new Date(),
          n = d.getTime(),
          newFileName = n + '.jpg';
    return newFileName;
  }

  copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(
      _ => {
        console.log('Success Copy : ' + _);
        this.updateStoredImages(newFileName);
      }, error => {
        console.log(error);
      }
    );
  }

  updateStoredImages(Name) {
    const FilePath = this.file.dataDirectory + Name;
    const resPath = this.pathForImg(FilePath);

    this.uploadImg = {
      name: Name,
      path: resPath,
      filePath: FilePath
    };

    console.log(this.uploadImg);
  }

  // Camera End

  transform(url: any) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
