import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AsetImagePage } from './aset-image.page';

const routes: Routes = [
  {
    path: '',
    component: AsetImagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AsetImagePageRoutingModule {}
