import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AsetImagePageRoutingModule } from './aset-image-routing.module';

import { AsetImagePage } from './aset-image.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AsetImagePageRoutingModule
  ],
  declarations: [AsetImagePage]
})
export class AsetImagePageModule {}
