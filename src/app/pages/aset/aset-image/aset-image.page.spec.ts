import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AsetImagePage } from './aset-image.page';

describe('AsetImagePage', () => {
  let component: AsetImagePage;
  let fixture: ComponentFixture<AsetImagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsetImagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AsetImagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
