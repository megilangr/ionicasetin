import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AsetService } from 'src/app/services/aset.service';
import { DomSanitizer } from '@angular/platform-browser';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { NavController, Platform } from '@ionic/angular';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-aset-image',
  templateUrl: './aset-image.page.html',
  styleUrls: ['./aset-image.page.scss'],
})
export class AsetImagePage implements OnInit {
  asetId: any = null;
  images: any = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private asetService: AsetService,
    private sanitizer: DomSanitizer,
    private photoViewer: PhotoViewer,
    private navCtrl: NavController,
    private loading: LoadingService,
    private plt: Platform,
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(
      param => {
        if (!param.has('asetId')) {
          this.navCtrl.navigateRoot('/aset');
        } else {
          this.asetId = param.get('asetId');
          this.loading.customLoading(3000);
        }
      }
    );
  }

  ionViewDidEnter() {
    setTimeout(() => {
      this.getImage();
    }, 1000);
  }

  getImage() {
    this.asetService.getImage(this.asetId).subscribe(
      data => {
        this.images = data;
        console.log(this.images);
      }
    );
  }

  detailImage(img) {
    this.plt.ready().then(() => {
      const options = {
        share: false,
        closeButton: true,
        copyToReference: true,
        headers: '',
        piccasoOptions: {}
      };
      this.photoViewer.show(img, 'Photo Aset', options);
    });
  }

  transform(url: any) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
