import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AsetListComponent } from './aset-list.component';

describe('AsetListComponent', () => {
  let component: AsetListComponent;
  let fixture: ComponentFixture<AsetListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsetListComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AsetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
