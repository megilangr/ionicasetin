import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-aset-list',
  templateUrl: './aset-list.component.html',
  styleUrls: ['./aset-list.component.scss'],
})
export class AsetListComponent implements OnInit {
  @Input() dataAset: any = null;
  @Input() hrefLink: any = null;

  link = '/aset/';

  constructor() { }

  ngOnInit() {}

  pilihAset(aset: any) {
    console.log(aset);
  }

}
