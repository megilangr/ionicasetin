import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AsetMovePage } from './aset-move.page';

const routes: Routes = [
  {
    path: '',
    component: AsetMovePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AsetMovePageRoutingModule {}
