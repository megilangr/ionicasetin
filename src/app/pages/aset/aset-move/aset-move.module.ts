import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AsetMovePageRoutingModule } from './aset-move-routing.module';

import { AsetMovePage } from './aset-move.page';
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AsetMovePageRoutingModule,
    IonicSelectableModule,
  ],
  declarations: [AsetMovePage]
})
export class AsetMovePageModule {}
