import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AsetMovePage } from './aset-move.page';

describe('AsetMovePage', () => {
  let component: AsetMovePage;
  let fixture: ComponentFixture<AsetMovePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsetMovePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AsetMovePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
