import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AsetService } from 'src/app/services/aset.service';
import { NavController, LoadingController, Platform } from '@ionic/angular';
import { AlertService } from 'src/app/services/alert.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { File } from '@ionic-native/file/ngx';
import { LoadingService } from 'src/app/services/loading.service';
import { IonicSelectableComponent } from 'ionic-selectable';
import { NgForm } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-aset-move',
  templateUrl: './aset-move.page.html',
  styleUrls: ['./aset-move.page.scss'],
})
export class AsetMovePage implements OnInit {
  segment: any;

  asetId: any;
  detailAset: any = {
    id: undefined,
    qr_code: undefined,
    owner_id: undefined,
    condition: undefined,
    serial_number: undefined,
    accounting_number: undefined,
    year_purchase: undefined,
    notes: undefined,
    asset_value: undefined,
    location_id: undefined,
    site_id: undefined,
    building_id: undefined,
    room_id: undefined,
    type_id: undefined,
    category_id: undefined,
    brand_id: undefined,
    model_id: undefined,
    client_id: undefined,
    is_active: undefined,
    created_by: undefined,
    updated_by: undefined,
    created_at: undefined,
    updated_at: undefined,
  };

  dataLocation: any;
  dataSite: any;
  dataBuilding: any;
  dataRoom: any;

  imageAset: any = null;
  uploadImg: any = null;

  base64Image: any;

  // validator
  Vimage: any = 0;
  Vlokasi: any = 0;
  Vsite: any = 0;
  Vbuilding: any = 0;
  Vroom: any = 0;

  constructor(
    private activatedRoute: ActivatedRoute,
    private asetService: AsetService,
    private navCtrl: NavController,
    private alert: AlertService,
    private loading: LoadingService,
    private loadingController: LoadingController,
    private camera: Camera,
    private base64: Base64,
    private file: File,
    private webView: WebView,
    private sanitizer: DomSanitizer,
    private plt: Platform
  ) {}

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.loading.customLoading(3000);
    this.loadingAwal();
  }
  paramCheck() {
    this.activatedRoute.paramMap.subscribe(
      paramMap => {
        if (!paramMap.has('asetId')) {
          return this.navCtrl.navigateRoot('/aset');
        } else {
          return this.asetId = paramMap.get('asetId');
        }
      }
    );
  }

  getDetail() {
    if (this.asetId !== undefined ) {
      this.asetService.detailAset(this.asetId).subscribe(
        data => {
          this.detailAset = data;
          this.asetService.getImage(this.asetId).subscribe(
            data1 => {
              let a: any = data1;
              if (a.length === 0) {
                this.imageAset = null;
              } else {
                this.imageAset = data1;
              }
            }
          );
        }
      );
    } else {
      this.alert.alertToast('Terjadi Kesalahan !');
      return this.navCtrl.navigateRoot('/aset');
    }
  }

  async loadingAwal() {
    const loading = await this.loadingController.create({
      message: 'Silahkan Tunggu..',
      spinner: 'lines',
      duration: 3000,
    });

    setTimeout(() => {
      this.paramCheck();
      this.getDetail();
      this.getLocation();
      this.segment = 'detail';
    }, 1000);

    return await loading.present();
  }

  getLocation() {
    if (this.asetId !== undefined ) {
      this.asetService.location().subscribe(
        data => {
          this.dataLocation = data;
        }
      );
    }
  }

  site(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    this.asetService.site(event.value.id).subscribe(
      data => {
        // this.Vlokasi = 1;
        this.dataSite = data;
        this.Vsite = 0;
        this.Vbuilding = 0;
        this.Vroom = 0;
      }
    );
  }

  building(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    this.asetService.building(event.value.id).subscribe(
      data => {
        // this.Vsite = 1;
        this.dataBuilding = data;
        this.Vbuilding = 0;
        this.Vroom = 0;
      }
    );
  }

  room(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    this.asetService.room(event.value.id).subscribe(
      data => {
        // this.Vbuilding = 1;
        this.dataRoom = data;
        this.Vroom = 0;
      }
    );
  }

  roomend(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    // this.Vroom = 1;
  }

  moveAset(form: NgForm) {
    // console.log(form.value);
    this.asetService.moveAset(
      this.asetId, form.value.locationAset.id, form.value.siteAset.id,
      form.value.buildingAset.id, form.value.roomAset.id, this.base64Image
    ).subscribe(
      data => {
        let a: any = data;
        this.alert.alertToast(a.message);
        this.navCtrl.navigateRoot('/aset');
        console.log(data);
      }
    );
  }

  segmentChanged(ev: any) {
    // console.log('Segment changed', ev.detail.value);
    this.segment = ev.detail.value;
  }



  // CAMERA

  openCamera() {
    this.plt.ready().then(() => {
      const options: CameraOptions = {
        quality: 50,
        destinationType: this.camera.DestinationType.FILE_URI,
        sourceType: this.camera.PictureSourceType.CAMERA,
        saveToPhotoAlbum: true
      };

      this.camera.getPicture(options).then(
        (imageData) => {
          const filePath: string = imageData;
          this.base64.encodeFile(filePath).then((base64File: string) => {
            this.base64Image = base64File;

            const currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
            const correctPath = imageData.substr(0, imageData.lastIndexOf('/') + 1);
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());

            this.Vimage = 1;

          }, (err) => {
            this.base64Image = null;
            this.Vimage = 0;
            this.alert.alertBox('Kesalahan !', '', 'Terjadi Kesalahan Saat Mengunggah File ! Error Code: ' + err);
          });
        }, (err) => {
          console.log('Terjadi Kesalahan ! Error -> ' + err);
        }
      );
    });
  }

  pathForImg(img) {
    if (img === null) {
      return '';
    } else {
      const converted = this.webView.convertFileSrc(img);
      console.log('Function Converted :' + converted);
      return converted;
    }
  }

  createFileName() {
    const d = new Date(),
          n = d.getTime(),
          newFileName = n + '.jpg';
    return newFileName;
  }

  copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(
      _ => {
        console.log('Success Copy : ' + _);
        this.updateStoredImages(newFileName);
      }, error => {
        console.log(error);
      }
    );
  }

  updateStoredImages(Name) {
    const FilePath = this.file.dataDirectory + Name;
    const resPath = this.pathForImg(FilePath);

    this.uploadImg = {
      name: Name,
      path: resPath,
      filePath: FilePath
    };

    console.log(this.uploadImg);
  }

  // Camera End

  transform(url: any) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
