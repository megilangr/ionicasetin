import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AsetPage } from './aset.page';
import { AuthGuard } from 'src/app/guard/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: AsetPage
  },
  {
    path: ':asetId',
    loadChildren: () => import('./aset-detail/aset-detail.module').then( m => m.AsetDetailPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: ':asetId/edit',
    loadChildren: () => import('./aset-edit/aset-edit.module').then( m => m.AsetEditPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'create/:qrCode',
    // path: 'create/id',
    loadChildren: () => import('./aset-create/aset-create.module').then( m => m.AsetCreatePageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'move/:asetId',
    loadChildren: () => import('./aset-move/aset-move.module').then( m => m.AsetMovePageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'image/:asetId',
    loadChildren: () => import('./aset-image/aset-image.module').then( m => m.AsetImagePageModule),
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AsetPageRoutingModule {}
