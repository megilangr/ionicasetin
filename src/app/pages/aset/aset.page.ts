import { Component, OnInit } from '@angular/core';
import { WorkorderService } from 'src/app/services/workorder.service';
import { AsetService } from 'src/app/services/aset.service';
import { NavController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingService } from 'src/app/services/loading.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-aset',
  templateUrl: './aset.page.html',
  styleUrls: ['./aset.page.scss'],
})
export class AsetPage implements OnInit {
  dataAset: any = null;
  dataWo: any = null;
  dataStatis: any = null;

  skip: any = 0;
  take: any = 20;
  hrefLink: any = null;

  constructor(
    private asetService: AsetService,
    private woService: WorkorderService,
    private navCtrl: NavController,
    private storage: Storage,
    private loading: LoadingService,
    private barcodeScanner: BarcodeScanner,
    private plt: Platform,
    private alert: AlertService,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.loading.customLoading(3000);
    if (this.dataWo == null) {
      this.storage.get('wo').then(
        data => {
          if (data == null) {
            console.log('navigate 01');
            this.navCtrl.navigateRoot('/workorder');
          } else {
            this.woService.wo = data;
          }
        }
      );
    }

    setTimeout(() => {
      //
    }, 2000);
  }

  ionViewDidEnter() {
    if (this.dataAset === null) {
      if (this.woService.wo === null) {
        console.log('navigate 02');
        this.navCtrl.navigateRoot('/workorder');
      } else {
        this.asetService.dataAset(0, 20).subscribe(
          data => {
            this.dataAset = data;
            this.dataStatis = data;
            console.log(data);
          }
        );
      }
    } else {
      this.loading.customLoad.dismiss();
    }
  }

  refreshData(event) {
    setTimeout(() => {
      if (this.woService.wo == null) {
        this.navCtrl.navigateRoot('/workorder');
      } else {
        this.asetService.dataAset(0, 20).subscribe(
          data => {
            this.dataAset = data;
            this.dataStatis = data;
            event.target.complete();
            this.skip = 0;
            this.take = 20;
            console.log(data);
          }
        );
      }
    }, 2000);
  }

  loadMore(event) {
    if (this.woService.wo === null) {
      this.navCtrl.navigateRoot('/workorder');
    } else {
      this.asetService.dataAset(this.skip + this.take, this.take).subscribe(
        data => {
          this.dataAset = this.dataAset.concat(data);
          this.dataStatis = this.dataAset;
          event.target.complete();
          // console.log(data);
          if (this.skip === 0) {
            this.skip = this.take + 20 + 1;
          } else {
            this.skip = this.skip + 20;
          }

          if (this.dataAset.length === 500) {
            event.target.disabled = true;
          }
        }
      );
    }

    setTimeout(() => {
      event.target.complete();
    }, 1000);
  }

  cari(evt) {
    let val = evt.srcElement.value;

    if (val !== '') {
      this.asetService.cariAset(0, 20, val).subscribe(
        data => {
          this.dataAset = data;
        }
      );
    } else {
      this.dataAset = this.dataStatis;
    }
  }

  scanQr() {
    this.plt.ready().then(() => {
      this.barcodeScanner.scan().then(data => {
        let a = data;
        if (a.cancelled === false) {
          this.asetService.qrCodeStatus(a.text).subscribe(
            data1 => {
              let b: any = data1;
              if (b.status === 'notfound') {
                this.alert.alertBox('Pemberitahuan !', '', b.message);
              } else if (b.status === 'idle') {
                this.alert.alertBox('Pemberitahuan !', '', b.message);
              } else if (b.status === 'used') {
                this.asetService.assetByQr(a.text).subscribe(
                  data2 => {
                    let aset: any = data2;
                    this.navCtrl.navigateRoot('/aset/' + aset.id);
                  }
                );
              } else if (b.status === 'booked') {
                this.navCtrl.navigateRoot('/aset/create/' + a.text);
              }
            }
          );
        }
      }).catch(err => {
        this.alert.alertToast('Terjadi Kesalahan - Scan Barcode Err-scanQrAts !');
        console.log(err);
      });
    });
  }

  generateLink() {
    if (this.woService.wo.status === undefined) {
      return this.navCtrl.navigateRoot('/workorder');
    } else if (this.woService.wo.status === 1) {
      return '/aset/';
    } else if (this.woService.wo.status === 2) {
      return '/aset/move/';
    } else {
      return '/aset/';
    }
  }

}
