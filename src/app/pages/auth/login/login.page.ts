import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform, NavController, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  tempToken: any = null;
  loading: any;

  email: any = null;
  password: any = null;
  
  constructor(
    private platform: Platform,
    private authService: AuthService,
    private alertService: AlertService,
    private storage: Storage,
    private loadingController: LoadingController,
    private navCtrl: NavController,
    private menu: MenuController
  ) { this.menu.enable(false); }

  ngOnInit() {
    this.platform.ready().then(() => {
      this.readyLoad();

      setTimeout(() => {
        this.email = 'client@asetin.id';
        this.password = '123456';
      }, 1000);

      if (this.authService.isLoggedIn) {
        this.navCtrl.navigateRoot('/landing');
      }
    });
  }

  async readyLoad() {
    const load = await this.loadingController.create({
      message: 'Mohon Tunggu...',
      duration: 500,
    });

    load.present();
  }

  login(form: NgForm) {
    this.showLoading();
    this.authService.login(form.value.email, form.value.password).subscribe(
      data => {
        this.tempToken = data;
        console.log(this.tempToken);
      }, error => {
        this.alertService.alertBox('Pemberitahuan !', '', 'E-mail / Password Salah !');
        this.loading.dismiss();
      }, () => {
        this.loading.dismiss();
        this.menu.enable(true);
        this.navCtrl.navigateRoot('/landing');
      }
    );
  }

  logout() {
    let token: any;
    this.storage.get('token').then(
      data => {
        token = data;
        this.authService.logout(data).subscribe(
          data1 => {
            console.log(data1);
            this.tempToken = null;
          }
        );
      }
    );
  }

  cekToken() {
    this.storage.get('token').then(
      data => {
        console.log(data);
      }
    );
  }

  async showLoading() {
    this.loading = await this.loadingController.create({
      message: 'Silahkan Tunggu...',
      duration: 5000,
    });
    await this.loading.present();
  }

}
