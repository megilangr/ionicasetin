import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AuthService } from 'src/app/services/auth.service';
import { MenuController } from '@ionic/angular';
import { WorkorderService } from 'src/app/services/workorder.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.scss'],
})
export class LandingPage implements OnInit {
  user: any = null;

  // workorders = {
  //   name: 'Workorder Belum Di-Pilih',
  //   location_name: '-',
  //   site_name: '-',
  //   building_name: '-',
  //   pilih: false,
  // };

  workorders: any = null;

  constructor(
    private menu: MenuController,
    private authService: AuthService,
    private woService: WorkorderService,
    private storage: Storage,
    private loading: LoadingService
  ) { this.menu.enable(true); }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.loading.normalLoad.present();
    this.user = this.authService.user;
    if (this.user == null) {
      this.authService.getUser().subscribe(
        data => {
          this.user = data;
        }
      );
    }

    if (this.workorders == null) {
      this.storage.get('wo').then(
        data => {
          if (data == null) {
            this.workorders = null;
          } else {
            this.workorders = data;
            console.log(data);
          }
          this.loading.normalLoad.dismiss();
        }
      );
    }
  }

  showToken() {
    setTimeout(() => {
      console.log(this.authService.token);
    }, 1000);
  }

}
