import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RequestCreatePageRoutingModule } from './request-create-routing.module';

import { RequestCreatePage } from './request-create.page';
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RequestCreatePageRoutingModule,
    IonicSelectableModule
  ],
  declarations: [RequestCreatePage]
})
export class RequestCreatePageModule {}
