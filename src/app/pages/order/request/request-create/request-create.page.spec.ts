import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RequestCreatePage } from './request-create.page';

describe('RequestCreatePage', () => {
  let component: RequestCreatePage;
  let fixture: ComponentFixture<RequestCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RequestCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
