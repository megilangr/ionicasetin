import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-request-create',
  templateUrl: './request-create.page.html',
  styleUrls: ['./request-create.page.scss'],
})
export class RequestCreatePage implements OnInit {
  order: any = {
    code: undefined,
    tanggal: undefined,
    notes: undefined,
  };

  dataSite: any = null;
  dataModel: any = null;

  constructor() { }

  ngOnInit() {
  }

}
