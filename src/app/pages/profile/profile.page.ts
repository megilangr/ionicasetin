import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AuthService } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';
import { NavController } from '@ionic/angular';
import { LoadingService } from 'src/app/services/loading.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  userName: string;
  userEmail: string;
  userStatus: string;
  userCreated: string;

  old: any = '';
  new: any = '';
  retype: any = '';

  constructor(
    private storage: Storage,
    private authService: AuthService,
    private alert: AlertService,
    private navCtrl: NavController,
    private loading: LoadingService,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.loading.customLoading(2000);
    setTimeout(() => {
      this.getUser();
    }, 1000);
  }

  getUser() {
    this.storage.get('user').then(data => {
      if (data) {
        this.userName = data['name'];
        this.userEmail = data['email'];
        this.userStatus = data['status'];
        this.userCreated = data['created_at'];
      }
    });
  }

  changePassword(form: NgForm) {
    this.authService.changePassword(form.value.old, form.value.new, form.value.retype).subscribe(
      data => {
        const res: any = data;

        if (res.status === 'wrong') {
          this.alert.alertBox('Gagal !', 'Ganti Kata Sandi', res.message);
        } else if (res.status === 'success') {
          form.reset();
          this.alert.alertBox('Berhasil !', 'Ganti Kata Sandi', res.message);
        } else if (res.status === 'notsame') {
          this.alert.alertBox('Kesalahan !', 'Ganti Kata Sandi', res.message);
        }
      }
    );
  }
}
