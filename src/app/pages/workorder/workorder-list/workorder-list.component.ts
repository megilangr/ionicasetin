import { Component, OnInit, Input } from '@angular/core';
import { Storage } from '@ionic/storage';
import { LoadingService } from 'src/app/services/loading.service';
import { AlertService } from 'src/app/services/alert.service';
import { NavController } from '@ionic/angular';
import { WorkorderService } from 'src/app/services/workorder.service';

@Component({
  selector: 'app-workorder-list',
  templateUrl: './workorder-list.component.html',
  styleUrls: ['./workorder-list.component.scss'],
})
export class WorkorderListComponent implements OnInit {
  @Input() dataWo: any;
  constructor(
    private storage: Storage,
    private loading: LoadingService,
    private alert: AlertService,
    private navCtrl: NavController,
    private woService: WorkorderService
  ) { }

  ngOnInit() {}

  pilihWo(data: any) {
    this.loading.normalLoad.present();
    this.storage.set('wo', data).then(
      () => {
        this.alert.alertToast('Workorder Di-Pilih !');
        this.woService.wo = data;
        setTimeout(() => {
          this.navCtrl.navigateRoot(['/aset']);
        }, 2000);
      }, error => {
        this.alert.alertToast('Terjadi Kesalahan !');
      }
    );
  }
}
