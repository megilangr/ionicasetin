import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WorkorderPageRoutingModule } from './workorder-routing.module';

import { WorkorderPage } from './workorder.page';
import { WorkorderListComponent } from './workorder-list/workorder-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WorkorderPageRoutingModule
  ],
  declarations: [WorkorderPage, WorkorderListComponent]
})
export class WorkorderPageModule {}
