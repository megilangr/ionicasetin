import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { WorkorderService } from 'src/app/services/workorder.service';
import { LoadingService } from 'src/app/services/loading.service';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-workorder',
  templateUrl: './workorder.page.html',
  styleUrls: ['./workorder.page.scss'],
})
export class WorkorderPage implements OnInit {
  dataWo: any = null;

  constructor(
    private woService: WorkorderService,
    private authService: AuthService,
    private loading: LoadingService,
    private alert: AlertService
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.dataWo = this.woService.getWo();
    this.loading.normalLoad.present();
    if (this.dataWo == null) {
      this.woService.getdataWo(this.authService.user.id).subscribe(
        data => {
          this.dataWo = data;
          console.log(this.dataWo);
          // this.loading.normalLoad.dismiss();
        }
      );
    }
  }

  refreshData(event) {
    setTimeout(() => {
      this.woService.getdataWo(this.authService.user.id).subscribe(
        data => {
          this.dataWo = data;
          event.target.complete();
        }, error => {
          // console.log('Terjadi Kesalahan !');
          this.alert.alertToast('Terjadi Kesalahan !');
          event.target.complete();
        }, () => {
          event.target.complete();
        }
      );
    }, 2000);
  }

}
