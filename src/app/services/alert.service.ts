import { Injectable } from '@angular/core';
import { ToastController, AlertController } from '@ionic/angular'; 

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(
    private toastController: ToastController,
    private alertController: AlertController
  ) { }

  async alertToast(message: any) {
    const toast = await this.toastController.create({
      message,
      duration: 2000,
      position: 'top',
      color: 'dark'
    });

    await toast.present();
  }

  async alertBox(header: any, subHeader: any, message: any) {
    const alert = await this.alertController.create({
      header,
      subHeader,
      message,
      buttons: ['Tutup']
    });

    await alert.present();
  }
}
