import { Injectable } from '@angular/core';
import { EnvService } from './env.service';
import { WorkorderService } from './workorder.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { tap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AsetService {
  aset: any = null;
  asetImage: any = null;
  wo: any = null;
  asetDetail: any = null;

  dataBrand: any = null;
  dataModel: any = null;
  dataLocation: any = null;
  dataSite: any = null;
  dataBuilding: any = null;
  dataRoom: any = null;
  dataOwner: any = null;

  checkQr: any = null;

  constructor(
    private http: HttpClient,
    private env: EnvService,
    private woService: WorkorderService,
    private authService: AuthService,
    private storage: Storage
  ) { }

  dataAset(skip: any, take: any) {
    const headers = new HttpHeaders({
      Authorization: this.authService.token['token_type'] + ' ' + this.authService.token['access_token']
    });

    return this.http.get(this.env.API_URL +
      'assets?skip=' + skip + '&take=' + take +
      '&building_id=' + this.woService.wo.building_id +
      '&wo_id=' + this.woService.wo.id + '&wo_status=' + this.woService.wo.status, {headers}).pipe(
        tap(data => {
          this.aset = data;
          return data;
        })
      );
  }

  createAset(
    qrCode: any,
    condition: any,
    serialNumber: any,
    accountingNumber: any,
    yearPurchase: any,
    notes: any,
    assetValue: any,
    brandId: any,
    modelId: any,
    roomId: any,
    ownerId: any,
    image: any
  ) {
    const headers = new HttpHeaders({
      Authorization: this.authService.token['token_type'] + ' ' + this.authService.token['access_token']
    });

    let data = {
      qr_code: qrCode,
      condition,
      serial_number: serialNumber,
      accounting_number: accountingNumber,
      year_purchase: yearPurchase,
      notes,
      asset_value: assetValue,
      location_id: this.woService.wo.location_id,
      site_id: this.woService.wo.site_id,
      building_id: this.woService.wo.building_id,
      room_id: roomId,
      brand_id: brandId,
      model_id: modelId,
      owner_id: ownerId,
      image,
    };

    return this.http.post(this.env.API_URL + 'assets', data, {headers}).pipe(
      tap(data => {
        let res = data;
        return res;
      })
    );
  }

  updateAset(
    id: any,
    qrCode: any,
    condition: any,
    serialNumber: any,
    accountingNumber: any,
    yearPurchase: any,
    notes: any,
    assetValue: any,
    brandId: any,
    modelId: any,
    roomId: any,
    ownerId: any,
    image: any) {
      const headers = new HttpHeaders({
        Authorization: this.authService.token['token_type'] + ' ' + this.authService.token['access_token']
      });

      return this.http.post(this.env.API_URL + 'assets/' + id, {
        qr_code: qrCode,
        condition,
        serial_number: serialNumber,
        accounting_number: accountingNumber,
        year_purchase: yearPurchase,
        notes,
        asset_value: assetValue,
        brand_id: brandId,
        model_id: modelId,
        room_id: roomId,
        owner_id: ownerId,
        image
      }, {headers}).pipe(
        tap(data => {
          let res = data;
          return res;
        }, err => {
          return console.log('error ! ' + err);
        })
      );
  }

  detailAset(asetId: any) {
    const headers = new HttpHeaders({
      Authorization: this.authService.token['token_type'] + ' ' + this.authService.token['access_token']
    });

    return this.http.get(this.env.API_URL + 'assets/' + asetId, {headers}).pipe(
      tap(data => {
        this.asetDetail = data;
        return this.asetDetail;
      })
    );
  }

  brand() {
    const headers = new HttpHeaders({
      Authorization: this.authService.token['token_type'] + ' ' + this.authService.token['access_token']
    });

    return this.http.get(this.env.API_URL + 'assetbrands/' + this.authService.user.id, { headers } ).pipe(
      tap(data => {
        this.dataBrand = data;
        return this.dataBrand;
      })
    );
  }

  model(brandId: any) {
    const headers = new HttpHeaders({
      Authorization: this.authService.token['token_type'] + ' ' + this.authService.token['access_token']
    });

    return this.http.get(this.env.API_URL + 'assetmodels/' + this.authService.user.id + '/' + brandId, {headers}).pipe(
      tap(data => {
        this.dataModel = data;
        return this.dataModel;
      })
    );
  }

  location() {
    const headers = new HttpHeaders({
      Authorization: this.authService.token['token_type'] + ' ' + this.authService.token['access_token']
    });

    return this.http.get(this.env.API_URL + 'locations/' + this.authService.user.id, {headers}).pipe(
      tap(data => {
        this.dataLocation = data;
        return this.dataLocation;
      })
    );
  }

  site(locationId: any) {
    const headers = new HttpHeaders({
      Authorization: this.authService.token['token_type'] + ' ' + this.authService.token['access_token']
    });

    return this.http.get(this.env.API_URL + 'sites/' + this.authService.user.id + '/' + locationId, {headers}).pipe(
      tap(data => {
        this.dataSite = data;
        return this.dataSite;
      })
    );
  }

  building(siteId: any) {
    const headers = new HttpHeaders({
      Authorization: this.authService.token['token_type'] + ' ' + this.authService.token['access_token']
    });

    return this.http.get(this.env.API_URL + 'buildings/' + this.authService.user.id + '/' + siteId, {headers}).pipe(
      tap(data => {
        this.dataBuilding = data;
        return this.dataBuilding;
      })
    );
  }

  room(buildingId: any) {
    const headers = new HttpHeaders({
      Authorization: this.authService.token['token_type'] + ' ' + this.authService.token['access_token']
    });

    return this.http.get(this.env.API_URL + 'rooms/' + this.authService.user.id + '/' + buildingId, {headers}).pipe(
      tap(data => {
        this.dataRoom = data;
        return this.dataRoom;
      })
    );
  }

  owner() {
    const headers = new HttpHeaders({
      Authorization: this.authService.token['token_type'] + ' ' + this.authService.token['access_token']
    });

    return this.http.get(this.env.API_URL + 'owners/' + this.authService.user.id, {headers}).pipe(
      tap(data => {
        this.dataOwner = data;
        return this.dataOwner;
      })
    );
  }

  cariAset(skip: number, take: number, search: string) {
    const headers = new HttpHeaders({
      Authorization: this.authService.token['token_type'] + ' ' + this.authService.token['access_token']
    });

    return this.http.get(
      this.env.API_URL + 'assets?skip=' + skip + '&take=' + take + '&building_id=' + this.woService.wo.building_id +
      '&wo_id=' + this.woService.wo.id + '&wo_status=' + this.woService.wo.status + '&search=' + search, {headers} 
    ).pipe(
      tap(data => {
        this.aset = data;
        return this.aset;
      })
    );
  }

  getImage(id: number){
    const headers = new HttpHeaders({
      Authorization: this.authService.token['token_type'] + ' ' + this.authService.token['access_token']
    });

    return this.http.get(
      this.env.API_URL + 'assets/' + id + '/images', {headers}
    ).pipe(
      tap(data => {
        this.asetImage = data;
        return this.asetImage;
      })
    );
  }

  qrCodeStatus(qrCode: any){
    const headers = new HttpHeaders({
      Authorization: this.authService.token['token_type'] + ' ' + this.authService.token['access_token']
    });

    return this.http.get(
      this.env.API_URL + 'qr/' + this.authService.user.client_id + '/' + this.woService.wo.building_id + '/' + qrCode, {headers}
    ).pipe(
      tap(data => {
        this.checkQr = data;
        return this.checkQr;
      })
    );
  }

  assetByQr(qrCode: any) {
    const headers = new HttpHeaders({
      Authorization: this.authService.token['token_type'] + ' ' + this.authService.token['access_token']
    });

    return this.http.get(
      this.env.API_URL + 'assets/' + qrCode + '/qr', {headers}
    ).pipe(
      tap(data => {
        let aset = data;
        return aset;
      })
    );
  }

  moveAset(asetId: any, locationId: any, siteId: any, buildingId: any, roomId: any, image64: any) {
    const headers = new HttpHeaders({
      'Authorization' : this.authService.token["token_type"] + " " + this.authService.token["access_token"]
    });

    const data = {
      location_id: locationId,
      site_id: siteId,
      building_id: buildingId,
      room_id: roomId,
      image: image64
    };

    return this.http.post(this.env.API_URL + 'assets/' + asetId + '/move', data, {headers}).pipe(
      tap(response => {
        return response;
      })
    );
  }

}
