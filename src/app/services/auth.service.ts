import { Injectable } from '@angular/core';
import { EnvService } from './env.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { tap } from 'rxjs/operators';
import { AlertService } from './alert.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn = false;
  token: any = null;
  user: any = null;

  grantType = 'password';
  clientId = '2';
  clientSecret = 'T6itLqoYfIaUCRQarWbImbNcmdtTWeKVA8wURtGA';
  postScope = '*';

  constructor(
    private env: EnvService,
    private http: HttpClient,
    private storage: Storage,
    private alert: AlertService
  ) { }

  login(email: string, password: string) {
    return this.http.post(this.env.API_URL + 'oauth/token', {
      grant_type : this.grantType,
      client_id : this.clientId,
      client_secret : this.clientSecret,
      username : email,
      password,
      scope : this.postScope
    }).pipe(
      tap(token => {
        this.storage.set('token', token).then(
          () => {
            console.log('Token Tersimpan !');
          }, error => {
            console.error('Terjadi Kesalahan Penyimpanan Token !');
          }
        );
        this.token = token;
        this.isLoggedIn = true;
        return token;
      }, error => {
        console.log(error);
        this.alert.alertBox('Error', '', error);
      })
    );
  }

  logout(token: any) {
    this.token = token;
    const headers = new HttpHeaders({
      Authorization: 'Bearer ' + token['access_token']
    });

    return this.http.get(this.env.API_URL + 'logout', {headers}).pipe(
      tap(data => {
        this.storage.clear();
        this.isLoggedIn = false;
        this.token = null;
        return data;
      }, err => {
        this.storage.clear();
        this.isLoggedIn = false;
        this.token = null;
        return err;
      })
    );
  }

  getToken() {
    this.storage.get('token').then(
      data => {
        this.token = data;

        if (this.token != null) {
          this.isLoggedIn = true;
          this.getUser().subscribe(
            data1 => {
            this.user = data1;
            }
          );
        } else {
          this.isLoggedIn = false;
        }
      }, error => {
        this.token = null;
        this.isLoggedIn = false;
      }
    );
  }

  getUser() {
    const headers = new HttpHeaders({
      Authorization: this.token['token_type'] + ' ' + this.token['access_token']
    });

    return this.http.get(this.env.API_URL + 'user', { headers }).pipe(
      tap(data => {
        this.storage.set('user', data);
        this.user = data;
        // console.log(this.user);
        return data;
      }, err => {
        this.logout(this.token);
      })
    );
  }

  changePassword(oldPass: any, newPass: any, retype: any) {
    const headers = new HttpHeaders({
      'Authorization' : this.token["token_type"] + " " + this.token["access_token"]
    });

    const data = {
      old_password: oldPass,
      new_password: newPass,
      confirm_password: retype
    };

    return this.http.post(this.env.API_URL + 'changepassword', data, {headers}).pipe(
      tap(
        response => {
          const res: any = response;
          return res;
        }
      )
    );
  }

}
