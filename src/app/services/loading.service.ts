import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  normalLoad: any = null;
  customLoad: any = null;

  constructor(
    private loadingController: LoadingController
  ) { }

  loadingSetup() {
    if (this.normalLoad == null) {
      this.normalLoading();
    }
  }

  async normalLoading() {
    this.normalLoad = await this.loadingController.create({
      message: 'Mempersiapkan Data...',
      duration: 3000
    });
  }

  async customLoading(duration: any) {
    this.customLoad = await this.loadingController.create({
      message: 'Mempersiapkan Data...',
      duration
    });

    return await this.customLoad.present();
  }
}
