import { Injectable } from '@angular/core';
import { EnvService } from './env.service';
import { AuthService } from './auth.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class WorkorderService {
  wo: any = null;
  dataWo: any = null;
	href: any = null;

  constructor(
    private env: EnvService,
    private storage: Storage,
    private authService: AuthService,
    private http: HttpClient
  ) { }

  getdataWo(id: number) {
    const headers = new HttpHeaders({
      Authorization: this.authService.token['token_type'] + ' ' + this.authService.token['access_token']
    });

    return this.http.get(this.env.API_URL + 'workorders/' + this.authService.user.id, { headers }).pipe(
      tap(data => {
        this.storage.set('dataWo', data);
        this.dataWo = data;
        return data;
      })
    );
  }

  getWo() {
    this.storage.get('wo').then(
      data => {
        this.wo = data;
        return data;
      }
    );
  }
}
